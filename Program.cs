﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using HP.HPTRIM.SDK;

namespace DoEDueDateProcessor
{
    class Program
    {
        static void Main(string[] args)
        {
            string dbID = "";
            string serverName = "";
            Database trimDB = null;
            TrimMainObjectSearch wfSrch = null;
            TrimMainObjectSearch activitySrch = null;

            if (ConfigurationManager.AppSettings["DBID"] != null)
                dbID = ConfigurationManager.AppSettings["DBID"];
            if (ConfigurationManager.AppSettings["WGServerName"] != null)
                serverName = ConfigurationManager.AppSettings["WGServerName"];

            trimDB = new Database();

            trimDB.WorkgroupServerName = serverName;
            trimDB.Id = dbID;
            trimDB.Connect();

            string wfDueDateProcessorCurrentActivityUDFName = ConfigurationManager.AppSettings["WFDueDateProcessorCurrentActivityUDF"];
            FieldDefinition WFDueDateProcessorCurrentActivityUDF = new FieldDefinition(trimDB, wfDueDateProcessorCurrentActivityUDFName);

            wfSrch = new TrimMainObjectSearch(trimDB, BaseObjectTypes.Workflow);
            wfSrch.SetSearchString("template:" + ConfigurationManager.AppSettings["WorkflowTemplateURI"] + " and not done");
            foreach (Workflow wf in wfSrch)
            {
                Activity wfCurrActivity = null;
                activitySrch = new TrimMainObjectSearch(trimDB, BaseObjectTypes.Activity);
                activitySrch.SetSearchString("workflow:" + wf.Uri + " and canStart");
                if (activitySrch.Count == 1)
                {
                    foreach (Activity tAct in activitySrch)
                    {
                        wfCurrActivity = tAct;
                    }
                }

                string currActivity = wf.GetFieldValueAsString(WFDueDateProcessorCurrentActivityUDF, StringDisplayType.Default, false);

                if (wfCurrActivity.Name.ToUpper() != currActivity.ToUpper())
                {
                    Workflow myWF = wf;
                    ProcessActivity(currActivity, wfCurrActivity, ref myWF);
                    wf.SetFieldValue(WFDueDateProcessorCurrentActivityUDF, new UserFieldValue(wfCurrActivity.Name));
                    wf.Save();
                }
            }

            trimDB.Disconnect();
            trimDB.Dispose();

        }

        private static bool ProcessActivity(string oldActivity, Activity pAct, ref Workflow wf)
        {
            Console.WriteLine("Workflow Name is: " + wf.Name);
            Console.WriteLine("oldActivity is " + oldActivity);
            Console.WriteLine("Current Activity is " + pAct.Name);
            string trimmedName = "";

            try
            {
                Record xRec = wf.Initiator;
                trimmedName = oldActivity;
                while (trimmedName.EndsWith(" !!"))
                {
                    trimmedName = trimmedName.Replace(" !!", "");
                }

                switch (trimmedName.ToLower())
                {
                    case "":
                        FieldDefinition dateDueMESUDF = new FieldDefinition(wf.Database, "Date Due to MES");
                        TrimDateTime xDueDate = wf.DateDue;
                        Console.WriteLine(xDueDate.ToString());
                        xRec.SetFieldValue(dateDueMESUDF, new UserFieldValue(xDueDate));
                        xRec.Save();
                        break;

                    case "new request received (deputy secretary's office)":
                        Console.WriteLine("Setting the UDF: Date Due to Deputy Secretary for Approval");
                        FieldDefinition dateDueDepSecOfficeUDF = new FieldDefinition(wf.Database, "Date Due to Deputy Secretary for Approval");
                        xRec.SetFieldValue(dateDueDepSecOfficeUDF, new UserFieldValue(wf.DateDue));
                        xRec.Save();
                        break;

                    case "new request received (executive director's office)":
                        Console.WriteLine("Setting the UDF: Date Due to Executive Director for Approval");
                        FieldDefinition dateDueExeutiveDirectorUDF = new FieldDefinition(wf.Database, "Date Due to Executive Director for Approval");
                        xRec.SetFieldValue(dateDueExeutiveDirectorUDF, new UserFieldValue(wf.DateDue));
                        xRec.Save();
                        break;

                    case "new request received (director's office)":
                        Console.WriteLine("Setting the UDF: Date Due to Director for Approval");
                        FieldDefinition dateDueDirectorUDF = new FieldDefinition(wf.Database, "Date Due to Director for Approval");
                        xRec.SetFieldValue(dateDueDirectorUDF, new UserFieldValue(wf.DateDue));
                        xRec.Save();
                        break;
                }

                trimmedName = pAct.Name;
                while (trimmedName.EndsWith(" !!"))
                {
                    trimmedName = trimmedName.Replace(" !!", "");
                }

                switch (trimmedName.ToLower())
                {
                    case "check if all child workflows complete":
                    case "end workflow":
                    case "draft response for deputy secretary's approval":
                    case "draft response for deputy secretary's approval !!":
                        Console.WriteLine("New Activity is " + trimmedName.ToLower());
                        FieldDefinition dateDueMESUDF = new FieldDefinition(wf.Database, "Date Due to MES");
                        TrimDateTime dateDue = xRec.GetFieldValue(dateDueMESUDF).AsDate();
                        Console.WriteLine("Date Due to MES is: " + dateDue.ToString());
                        Console.WriteLine("Workflow Date Due is: " + wf.DateDue.ToString());
                        if (wf.DateDue != dateDue)
                        {
                            Console.WriteLine("Setting Workflow date Due to: " + dateDue.ToString());
                            wf.DateDue = dateDue;
                            wf.Save();
                        }
                        break;

                    case "draft response for executive director's approval":
                    case "draft response for executive director's approval !!":
                        Console.WriteLine("New Activity is draft response for executive director's approval");
                        FieldDefinition dateDueDepSecOfficeUDF = new FieldDefinition(wf.Database, "Date Due to Deputy Secretary for Approval");
                        dateDue = xRec.GetFieldValue(dateDueDepSecOfficeUDF).AsDate();
                        if (dateDue.IsClear)
                        {
                            dateDueMESUDF = new FieldDefinition(wf.Database, "Date Due to MES");
                            dateDue = xRec.GetFieldValue(dateDueMESUDF).AsDate();
                        }

                        Console.WriteLine("Date Due to Deputy Secretary for Approval to MES is: " + dateDue.ToString());
                        Console.WriteLine("Workflow Date Due is: " + wf.DateDue.ToString());
                        if (!dateDue.IsClear)
                            if (wf.DateDue != dateDue)
                            {
                                Console.WriteLine("Setting Workflow date Due to: " + dateDue.ToString());
                                wf.DateDue = dateDue;
                                wf.Save();
                            }
                        break;

                    case "draft response for director's approval":
                    case "draft response for director's approval !!":
                        Console.WriteLine("New Activity is draft response for director's approval");
                        FieldDefinition dateDueExecutiveDirectorOfficeUDF = new FieldDefinition(wf.Database, "Date Due to Executive Director for Approval");
                        dateDue = xRec.GetFieldValue(dateDueExecutiveDirectorOfficeUDF).AsDate();
                        if (dateDue.IsClear)
                        {
                            dateDueDepSecOfficeUDF = new FieldDefinition(wf.Database, "Date Due to Deputy Secretary for Approval");
                            dateDue = xRec.GetFieldValue(dateDueDepSecOfficeUDF).AsDate();
                            if (dateDue.IsClear)
                            {
                                dateDueMESUDF = new FieldDefinition(wf.Database, "Date Due to MES");
                                dateDue = xRec.GetFieldValue(dateDueMESUDF).AsDate();
                            }
                        }

                        Console.WriteLine("Date Due to Executive Director for Approval to MES is: " + dateDue.ToString());
                        Console.WriteLine("Workflow Date Due is: " + wf.DateDue.ToString());
                        if (!dateDue.IsClear)
                            if (wf.DateDue != dateDue)
                            {
                                Console.WriteLine("Setting Workflow date Due to: " + dateDue.ToString());
                                wf.DateDue = dateDue;
                                wf.Save();
                            }
                        break;

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in ProcessActivity is " + ex.Message);
            }

            return true;
        }

    }


}
